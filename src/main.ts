import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const configService = app.get(ConfigService)

  app.useGlobalPipes(new ValidationPipe({
    transform: true
  }));
  
  const config = new DocumentBuilder()
    .setTitle('Count Access')
    .setDescription('Contagem de acessos a um site')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  
  await app.listen(configService.get('PORT'));
}
bootstrap();
