import * as mongoose from 'mongoose'

export const UserSchema = new mongoose.Schema({

    nomeUsuario: String,
    username: String,
    email: String, 
    password: String,

}, { timestamps: true, collection: 'users' });