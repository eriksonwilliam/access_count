import { Document } from 'mongoose'
import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString, IsEmail } from 'class-validator';
import { Exclude, Expose } from 'class-transformer';

export class User {
    
    @IsNotEmpty({
        message: 'nomeDeUsuario cannot be empty.'
    })
    @IsString({
        message: 'nomeDeUsuario has to be String.'
    })  
    @ApiProperty()
    nomeUsuario: string;
    
    @IsNotEmpty({
        message: 'username cannot be empty.'
    })
    @IsString({
        message: 'username has to be String.'
    })  
    @ApiProperty()
    username: string;
    
    @IsEmail({}, {
        message: 'email must be a valid email address.'
    })
    @IsNotEmpty({
        message: 'email cannot be empty.'
    })
    @ApiProperty()
    email: string;
    
    @Exclude({
        toPlainOnly: true
    })
    @IsNotEmpty({
        message: 'password cannot be empty.'
    })
    @IsString({
        message: 'password has to be String.'
    })
    @ApiProperty()
    password: string;
}