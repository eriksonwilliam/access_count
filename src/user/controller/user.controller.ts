import { Body, Controller, Post } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { User } from '../models/user.interface';
import { UserService } from '../service/user.service';

@Controller('users')
@ApiTags("Users")
export class UserController { 

    constructor(private userService: UserService) {}

    @Post()
    @ApiBody({ type: User})
    public cria(@Body() user: User): User {
        const userCreated = this.userService.cria(user);

        return userCreated;
    }
}
