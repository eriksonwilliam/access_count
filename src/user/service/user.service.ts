import { Injectable } from '@nestjs/common';
import { User } from '../models/user.interface';

@Injectable()
export class UserService {

    private users = []

    public cria(user: User): User {
        this.users.push(user);
        
        return user;
    }
}
